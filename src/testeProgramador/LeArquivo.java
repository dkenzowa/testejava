package testeProgramador;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class LeArquivo {

	public static String converteTempo(int n) {
		String converteTempo = Integer.toString(n);
		StringBuilder sb = new StringBuilder(converteTempo);
		sb.insert(1, ":");
		sb.insert(4, ".");
		return sb.toString();
	}

	public static void main(String[] args) {
		try {
			Scanner in = new Scanner(new File("C:\\Users\\danik\\OneDrive\\�rea de Trabalho\\arquivo_de_entrada.txt"),
					"UTF-8");
			FileWriter fileWriter = new FileWriter(
					"C:\\Users\\danik\\OneDrive\\�rea de Trabalho\\ResultadoCorrida.txt");
			PrintWriter printWriter = new PrintWriter(fileWriter);

			in.nextLine();

			String[] separador = null;
			String line = null;
			List<Integer> tempoTotalCorredores = new ArrayList<>();
			int somaMassa = 0;
			int somaBarri = 0;
			int somaRaikko = 0;
			int somaWebber = 0;
			int somaAlonso = 0;
			int somaVettel = 0;

			while (in.hasNextLine()) {
				line = in.nextLine();

				separador = line.split("\\s+");

				int voltas = Integer.parseInt(separador[4]);

				String tempo = separador[5].replaceAll("[^0-9]", "");
				int tempoTotal = Integer.parseInt(tempo);

				if (line.contains("038")) {
					tempoTotalCorredores.add(tempoTotal);
					if (voltas == 4) {
						for (int i = 0; i < voltas; i++) {
							somaMassa = somaMassa + tempoTotal;
						}
					}
				}

				if (line.contains("033")) {
					tempoTotalCorredores.add(tempoTotal);
					if (voltas == 4) {
						for (int i = 0; i < voltas; i++) {
							somaBarri = somaBarri + tempoTotal;
						}
					}
				}

				if (line.contains("002")) {
					tempoTotalCorredores.add(tempoTotal);
					if (voltas == 4) {
						for (int i = 0; i < voltas; i++) {
							somaRaikko = somaRaikko + tempoTotal;
						}
					}
				}

				if (line.contains("023")) {
					tempoTotalCorredores.add(tempoTotal);
					if (voltas == 4) {
						for (int i = 0; i < voltas; i++) {
							somaWebber = somaWebber + tempoTotal;
						}
					}
				}

				if (line.contains("015")) {
					tempoTotalCorredores.add(tempoTotal);
					if (voltas == 4) {
						for (int i = 0; i < voltas; i++) {
							somaAlonso = somaAlonso + tempoTotal;
						}
					}
				}

				if (line.contains("011")) {
					tempoTotalCorredores.add(tempoTotal);
					if (voltas == 3) {
						for (int i = 0; i < voltas; i++) {
							somaVettel = somaVettel + tempoTotal;
						}
					}
				}

				if (voltas == 4 || line.contains("011") && voltas == 3) {

						List<Integer> list = Arrays.asList( somaMassa, somaBarri, somaRaikko, somaWebber, somaAlonso );
						Collections.sort(list);

						for (int i = 0; i < list.size(); i++) {
							if (list.get(i).equals(somaBarri)) {
								printWriter.print("Posi��o chegada: " + i + "�\n");
								break;
							}

							if (list.get(i).equals(somaBarri)) {
								printWriter.print("Posi��o chegada: " + i + "�\n");
								break;
							}

							if (list.get(i).equals(somaRaikko)) {
								printWriter.print("Posi��o chegada: " + i + "�\n");
								break;
							}

							if (list.get(i).equals(somaWebber)) {
								printWriter.print("Posi��o chegada: " + i + "�\n");
								break;
							}

							if (list.get(i).equals(somaAlonso)) {
								printWriter.print("Posi��o chegada: " + i + "�\n");
								break;
							}
						}

					printWriter.print("C�digo Piloto: " + separador[1] + ";\n" + "Nome do Piloto: " + separador[3]
							+ ";\n" + "Quantidade voltas completadas: " + voltas + ";\n");

					if (line.contains("038")) {
						printWriter.print("Tempo total de prova: " + converteTempo(somaMassa) + ";\n");
					}

					if (line.contains("033")) {
						printWriter.print("Tempo total de prova: " + converteTempo(somaBarri) + ";\n");
					}

					if (line.contains("002")) {
						printWriter.print("Tempo total de prova: " + converteTempo(somaRaikko) + ";\n");
					}

					if (line.contains("023")) {
						printWriter.print("Tempo total de prova: " + converteTempo(somaWebber) + ";\n");
					}

					if (line.contains("015")) {
						printWriter.print("Tempo total de prova: " + converteTempo(somaAlonso) + ";\n");
					}

					if (line.contains("011") && voltas == 3) {
						printWriter.print("Tempo total de prova: " + converteTempo(somaVettel) + ";\n");
					}
				}
			}

			in.close();

			printWriter.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
